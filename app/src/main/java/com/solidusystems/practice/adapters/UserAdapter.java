package com.solidusystems.practice.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.solidusystems.practice.R;
import com.solidusystems.practice.databinding.ItemUserBinding;
import com.solidusystems.practice.models.User;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private final List<User> users;

    public UserAdapter (List<User> users) {
        this.users = users;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        final ItemUserBinding itemUserBinding = ItemUserBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new UserViewHolder(itemUserBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        final User user = this.users.get(position);
        holder.bind(user);
    }

    @Override
    public int getItemCount() {
        return this.users.size();
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        private final ItemUserBinding itemUserBinding;

//        private final TextView txvId;
//        private final TextView txvName;
//        private final TextView txvUsername;
//        private final TextView txvEmail;
        public UserViewHolder(@NonNull ItemUserBinding itemView) {
            super(itemView.getRoot());
            this.itemUserBinding = itemView;
//            this.txvId = itemView.findViewById(R.id.txv_id);
//            this.txvName = itemView.findViewById(R.id.txv_name);
//            this.txvUsername = itemView.findViewById(R.id.txv_username);
//            this.txvEmail = itemView.findViewById(R.id.txv_email);
        }

        private void bind (User user) {
            this.itemUserBinding.setUser(user);
        }
    }
}
