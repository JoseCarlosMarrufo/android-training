package com.solidusystems.practice.viewmodels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.solidusystems.practice.data.local.DataManager;
import com.solidusystems.practice.models.User;

import java.util.List;

public class UserViewModel extends AndroidViewModel {

    private final MutableLiveData<List<User>> userLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> test = new MutableLiveData<>();

    public UserViewModel(@NonNull Application application) {
        super(application);
    }

    public void getUser () {
        final List<User> users = DataManager.getUsers();
        userLiveData.setValue(users);
    }

    public LiveData<List<User>> getUsers () {
        return userLiveData;
    }

}
