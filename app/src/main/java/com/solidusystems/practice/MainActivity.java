package com.solidusystems.practice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.solidusystems.practice.adapters.UserAdapter;
import com.solidusystems.practice.data.local.DataManager;
import com.solidusystems.practice.databinding.ActivityMainBinding;
import com.solidusystems.practice.models.User;
import com.solidusystems.practice.viewmodels.UserViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private ActivityMainBinding mainBinding;
    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        this.userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        this.mainBinding.txvTitle.setText("Hola abelito");
        getUsers();
        observeUsers();
    }

    private void observeUsers() {
        this.userViewModel.getUsers().observe(this, users -> {
            if (users != null) {
                this.mainBinding.userRecyclerview.setAdapter(new UserAdapter(users));
            }
        });
    }

    private void getUsers() {
        showProgressBar(true);
        new Handler().postDelayed(() -> {
            showProgressBar(false);
            this.userViewModel.getUser();
        }, 3000);

    }

    private void showProgressBar (boolean isVisible) {
       this.mainBinding.setIsLoading(isVisible);
    }
}