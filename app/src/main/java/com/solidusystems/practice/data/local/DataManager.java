package com.solidusystems.practice.data.local;

import com.solidusystems.practice.models.User;

import java.util.ArrayList;
import java.util.List;

public class DataManager {

    public static List<User> getUsers () {
        return new ArrayList<User>(){{
            add(new User(1, "José Carlos", "Carlos", "carlos@gmail.com"));
            add(new User(2, "Maria Lopez", "Mari", "maria@gmail.com"));
            add(new User(3, "Juan Fernando", "Juan", "juan@gmail.com"));
            add(new User(4, "Ernesto Perez", "Ernesto", "ernesto@gmail.com"));
            add(new User(5, "Luis Gonzalez", "Luis", "luis@gmail.com"));
            add(new User(6, "Karla Lopez", "Karla", "karla@gmail.com"));
            add(new User(7, "Gabriela Hernandez", "Gabriela", "gaby@gmail.com"));
        }};
    }
}
